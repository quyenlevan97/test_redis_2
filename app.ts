import express from 'express';
import redis from 'redis';
import axios from 'axios';
import logger from 'morgan';
import dotenv from 'dotenv-safe';
import { promisify } from 'util';



const app = express();
app.use(logger('dev'));
const PORT =process.env.PORT || 6116;

const client=redis.createClient({
    host:'localhost',
    port: 3456
});


const GET_ASYNC=promisify(client.get).bind(client);
const SET_ASYNC=promisify(client.set).bind(client);
app.get("/",(req,res)=>{
    console.log("port",process.env.PORT)
    return res.send(process.env.PORT);
})
app.get('/users', async (req, res) => {
    try{
        
        const reply = GET_ASYNC('users');
        if(reply){
            console.log("data catching on redis:");
            return res.send(reply);
            
        }
        const response= await axios.get('https://api.github.com/users');
        const saveResult= SET_ASYNC('users',JSON.stringify(response.data));
        return res.send(response.data);
    }catch(error){
        res.send(error);
    }
})

app.listen(PORT, () => {
    console.log(`Started At Port : ${PORT}`);
})